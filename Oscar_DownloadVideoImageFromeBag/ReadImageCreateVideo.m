% Sebastien Petit 
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% This script can read image and create video with rosbag file

clear all
close all;
path(path, '../read_bags');
path(path, '../helper_functions');

%% get camera image/video from kalibr
bagfile =  '../bags/pitch.bag';
topic_image = '/camera/fisheye1/image_raw'; % if topic change we must change code
bag = ros.Bag(bagfile);
bag.info
image_data_raw = bag.readAll(topic_image);

%% define parameters accordance with topics convention
imgData = image_data_raw{1,200}.data;
imgH = image_data_raw{1,1}.height;
imgW = image_data_raw{1,1}.width;
img = double(reshape(imgData(1:end),imgW,imgH)'); %Reshape image data

%% warning : different topic so for example if RGB image different convention
% img = zeros(imgW,imgH,3);
% image(img);
% imgR = reshape(imgData(1:3:end),imgW,imgH)';
% imgG = reshape(imgData(2:2:end),imgW,imgH)';
% imgB = reshape(imgData(3:3:end),imgW,imgH)';
% img(:,:,1) = imgR;
% img(:,:,2) = imgG;
% img(:,:,3) = imgB;

%% rescale and see image
rescaledMatrix = rescale(img, 0, 255);
rescaledMatrix = uint8(rescaledMatrix); 
figure; 
imshow(rescaledMatrix, []);

%% create video 
video = VideoWriter('yourvideo.avi'); %create the video object
open(video); %open the file for writing
N = length(image_data_raw);
    img = zeros(imgH,imgW);
    
for ii=1:N %where N is the number of images
    imgData = image_data_raw{1,ii}.data;
    img = double(reshape(imgData(1:end),imgW,imgH)');
    rescaledMatrix = rescale(img, 0, 255);
    rescaledMatrix = uint8(rescaledMatrix);
    imwrite(uint8(rescaledMatrix), 'NewFile.jpg')
    I = imread('NewFile.jpg'); %read the next image
    writeVideo(video,I); %write the image to file
end
close(video); 
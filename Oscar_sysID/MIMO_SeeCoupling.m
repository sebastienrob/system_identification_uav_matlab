%% Estimate the Whole System as 2-input 2-output MIMO System
% *The purpose here is to see of there is coupling*

Experiment2.Ts = Experiment1.Ts;    
Data1 = iddata([Experiment1.y1', Experiment1.y2'], ...
    [Experiment1.u1', Experiment1.u2'], Experiment1.Ts, ...
    'ExperimentName', 'OscarSysID_1', ...
    'InputName',{'roll_{cmd}','pitch_{cmd}'},...
    'OutputName',{'roll','pitch'}', ...
    'InputUnit',{'rad', 'rad'},...
    'OutputUnit',{'rad', 'rad'},...
    'TimeUnit','Second');

Data2 = iddata([Experiment2.y1', Experiment2.y2'], ...
    [Experiment2.u1', Experiment2.u2'], Experiment2.Ts, ...
    'ExperimentName', 'OscarSysID_2', ...
    'InputName',{'roll_{cmd}','pitch_{cmd}'},...
    'OutputName',{'roll','pitch'}', ...
    'InputUnit',{'rad', 'rad'},...
    'OutputUnit',{'rad', 'rad'}, ...
    'TimeUnit','Second');

MergedData = merge(Data1, Data2);

%np = 2;
nz = 0;
Full_estimated_tf = tfest(MergedData, np,nz);

figure;
bodemag(Full_estimated_tf);

%% Estimated Transfer Functions

disp('Roll estimated transfer function is: ');
tf(roll_estimated_tf)

if(np==1)
    roll_params=getpvec(roll_estimated_tf);
    roll_gain=roll_params(1)/roll_params(2);
    roll_tau=1/roll_params(2);
    fprintf('roll tau=%.3f, gain=%.3f\n',roll_tau,roll_gain);
elseif(np==2)
    roll_params=getpvec(roll_estimated_tf);
    roll_omega=sqrt(roll_params(3));
    roll_gain=roll_params(1)/roll_params(3);
    roll_damping=roll_params(2)/(2*roll_omega);
    fprintf('roll omega=%.3f, gain=%.3f damping=%.3f\n',roll_omega,roll_gain,roll_damping);
end

figure('Name','System analysis (roll)');
subplot(311);
bode(roll_estimated_tf); grid;
title('Roll bode plot');

subplot(312);
%rlocusplot(roll_estimated_tf); grid;
title('Roll RootLucas plot');

subplot(313);
step(roll_estimated_tf); grid;
title('Roll step response plot');

disp('Pitch estimated transfer function is: ');
tf(pitch_estimated_tf)

if(np==1)
    pitch_params=getpvec(pitch_estimated_tf);
    pitch_gain=pitch_params(1)/pitch_params(2);
    pitch_tau=1/pitch_params(2);
    fprintf('pitch tau=%.3f, gain=%.3f\n',pitch_tau, pitch_gain);
elseif(np==2)
    pitch_params=getpvec(pitch_estimated_tf);
    pitch_omega=sqrt(pitch_params(3));
    pitch_gain=pitch_params(1)/pitch_params(3);
    pitch_damping=pitch_params(2)/(2*pitch_omega);
    fprintf('pitch omega=%.3f, gain=%.3f damping=%.3f\n',pitch_omega,pitch_gain,pitch_damping);
end

figure('Name','System analysis (pitch)');
subplot(311);
bode(pitch_estimated_tf); grid;
title('Pitch bode plot');

subplot(312);
%rlocusplot(pitch_estimated_tf); grid;
title('Pitch RootLucas plot');

subplot(313);
step(pitch_estimated_tf); grid;
title('Pitch step response plot');

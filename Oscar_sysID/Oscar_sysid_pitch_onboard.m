% Sebastien Petit 
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% root on git clone https://github.com/ethz-asl/mav_system_identification -b DJI_M100_sysID
% This script can identified dynamic of roll with two sets of data PITCH
% DJI vc channel, 1=pitch, 2=roll, 4=vertical velocity, 3=yaw_rate.
% rpy = euler angle
% use xcorr if delay between datasets

clear all
close all;
clc;
path(path, '../read_bags');
path(path, '../helper_functions');

%% Assume 1st  order system (!!!choice order!!!)  
np = 1; % number of poles [2]
nz = 0; % number of zeros [1]

%% two experiments are needed to validate the identification
bagfile_exp1 =  '../bags/pitch.bag'; % give name of bag
bagfile_exp2 =  '../bags/pitch.bag';

topic_imu = '/dji_ros/imu'; % find name of topic
topic_vcdata = '/dji_ros/rc';

bag1 = ros.Bag(bagfile_exp1); % read bag
bag2 = ros.Bag(bagfile_exp2);

bag1.info % get info of bag (topic, variables...)
bag2.info

%% Prepare datasets / extract data from Ros bag
Experiment1.IMU = readImu(bag1, topic_imu);
Experiment1.RCData = readRCDataFromJoyMessage(bag1, topic_vcdata);
Experiment2.IMU = readImu(bag2, topic_imu);
Experiment2.RCData = readRCDataFromJoyMessage(bag2, topic_vcdata);

%% Write the quaternions properly
Experiment1.IMU.q = [Experiment1.IMU.q(4,:); Experiment1.IMU.q(1,:); Experiment1.IMU.q(2,:); Experiment1.IMU.q(3,:)];
Experiment2.IMU.q = [Experiment2.IMU.q(4,:); Experiment2.IMU.q(1,:); Experiment2.IMU.q(2,:); Experiment2.IMU.q(3,:)];

%% time from 0 EXP 1 and EXP 2
Experiment1.RCData.t = Experiment1.RCData.t - Experiment1.RCData.t(1);
Experiment1.IMU.t = Experiment1.IMU.t - Experiment1.IMU.t(1);

Experiment2.RCData.t = Experiment2.RCData.t - Experiment2.RCData.t(1);
Experiment2.IMU.t = Experiment2.IMU.t - Experiment2.IMU.t(1);

%% INTERP cause no same size (IMU=~3000 and IMU =~12000) 
%% RCDATA interpolation with respect to IMU EXP 1
Experiment1.RCData.pitch = Experiment1.RCData.axes(2,:);
cmd_pitch_intp = interp1(Experiment1.RCData.t,Experiment1.RCData.pitch,Experiment1.IMU.t,'spline');
Experiment1.IMU.q = [Experiment1.IMU.q(1,:);Experiment1.IMU.q(2,:);Experiment1.IMU.q(3,:);Experiment1.IMU.q(4,:)];
Experiment1.RCData.pitch = cmd_pitch_intp;
Experiment1.RCData.t = zeros(1,size(cmd_pitch_intp,2));
Experiment1.RCData.t = Experiment1.IMU.t;
Experiment1.rpy_imu = quat2rpy(Experiment1.IMU.q);

%% RCDATA interpolation with respect to IMU EXP 2
Experiment2.RCData.pitch = Experiment2.RCData.axes(2,:);
cmd_pitch_intp = interp1(Experiment2.RCData.t,Experiment2.RCData.pitch,Experiment2.IMU.t,'spline');
Experiment2.IMU.q = [Experiment2.IMU.q(1,:);Experiment2.IMU.q(2,:);Experiment2.IMU.q(3,:);Experiment2.IMU.q(4,:)];
Experiment2.RCData.pitch = cmd_pitch_intp;
Experiment2.RCData.t = zeros(1,size(cmd_pitch_intp,2));
Experiment2.RCData.t = Experiment2.IMU.t;
Experiment2.rpy_imu = quat2rpy(Experiment2.IMU.q);

%% scaling (lool "scalingAll.m") - delete transitory (no take begin and end part)
% specific scale of remote control is compute before Matlab process here 
DisplayPlot = 1; % before scale_roll = 25 * pi/180
[scale_pitch] = scalingAll(Experiment2.RCData.pitch, Experiment2.rpy_imu(2,:),Experiment2.RCData.t, Experiment2.IMU.t, DisplayPlot);

Experiment1.pitch_cmd   = (Experiment1.RCData.pitch) * scale_pitch;
Experiment2.pitch_cmd   = (Experiment2.RCData.pitch) * scale_pitch; % use same scale

%% Plot attitude from experiment 1
figure;
subplot(2,1,1);
plot(Experiment1.IMU.t, Experiment1.rpy_imu(2,:)*180/pi, Experiment1.RCData.t, Experiment1.pitch_cmd*180/pi,'g--', 'linewidth', 2);
xlabel('time');
ylabel('pitch [deg]');
title('pitch from IMU and cmd(after regression/scaling) EXP 1');

%% Plot attitude from experiment 2
subplot(2,1,2);
plot(Experiment2.IMU.t, Experiment2.rpy_imu(2,:)*180/pi, Experiment2.RCData.t, Experiment2.pitch_cmd*180/pi, 'g--', 'linewidth', 2);
xlabel('time');
ylabel('pitch [deg]');
title('pitch from IMU and cmd(after regression/scaling) EXP 2');

%% Identification of roll system
% Control parameters
delay=[]; NaN; % [] menas no time delay or NaN for enabling delay estimation.

%% The length of data may vary. (1)-
Experiment1.t = Experiment1.RCData.t;
Experiment1.u2 = Experiment1.pitch_cmd;
Experiment1.y2 = Experiment1.rpy_imu(2,:);
Experiment1.Ts = mean(diff(Experiment1.t)); % sample time

%% get rid of first and last 10 seconds (to remove ground and transient effects)
Experiment1.u2 = Experiment1.u2(Experiment1.t>10 & Experiment1.t < Experiment1.t(end)-10);
Experiment1.y2 = Experiment1.y2(Experiment1.t>10 & Experiment1.t < Experiment1.t(end)-10);
Experiment1.t = Experiment1.t(Experiment1.t>10 & Experiment1.t < Experiment1.t(end)-10);

pitch_data1 = iddata(Experiment1.y2',Experiment1.u2',Experiment1.Ts,...
    'ExperimentName', 'OscarSysID_1', 'InputName','pitch_{cmd}',...
    'OutputName','pitch', 'InputUnit','rad', 'OutputUnit','rad',...
    'TimeUnit','Second');

%% remove any trend in the data
pitch_data1 = detrend(pitch_data1);

%% The length of data may vary. (2)
Experiment2.t = Experiment2.RCData.t;
Experiment2.u2 = Experiment2.pitch_cmd;
Experiment2.y2 = Experiment2.rpy_imu(2,:);
Experiment2.Ts = mean(diff(Experiment2.t));

%% get rid of first and last 10 seconds (to remove ground and transient effects)
Experiment2.u2 = Experiment2.u2(Experiment2.t>10 & Experiment2.t < Experiment2.t(end)-10);
Experiment2.y2 = Experiment2.y2(Experiment2.t>10 & Experiment2.t < Experiment2.t(end)-10);
Experiment2.t = Experiment2.t(Experiment2.t>10 & Experiment2.t < Experiment2.t(end)-10);

pitch_data2 = iddata(Experiment2.y2',Experiment2.u2',Experiment2.Ts, ...
    'ExperimentName', 'OscarSysID_2', 'InputName','pitch_{cmd}',...
    'OutputName','pitch', 'InputUnit','rad', 'OutputUnit','rad', ...
    'TimeUnit','Second');
pitch_data2 = detrend(pitch_data2);   

%% Generate model using Experiment1 and validate the model with Experiment2
pitch_estimated_tf1 = tfest(pitch_data1,np, nz,delay); % uncomment for see transfert function
[~, fit1, ~] = compare(pitch_data2, pitch_estimated_tf1);

%% Generate model using Experiment2 and validate the model with Experiment1
pitch_estimated_tf2 = tfest(pitch_data2,np, nz,delay); % uncomment for see transfert function
[~, fit2, ~] = compare(pitch_data1, pitch_estimated_tf2);

if fit1>fit2
    %% We pick the first Identification
    pitch_estimated_tf = pitch_estimated_tf1;
    disp('The pitch model is estimated using experiment 1 and validated on data from experiment 2');
    figure;
    compare(pitch_data2, pitch_estimated_tf1);
    disp(strcat('The pitch model fits the validation data with **', ...
        num2str(fit1), '** %'));
else
    %% We pick the second Identification
    pitch_estimated_tf = pitch_estimated_tf2;
    disp('The pitch model is estimated using experiment 2 and validated on data from experiment 1');
    figure;
    compare(pitch_data1, pitch_estimated_tf2);
    disp(strcat('The pitch model fits the validation data with **', ...
        num2str(fit2), '** %'));
end

%% Estimated Transfer Functions
disp('pitch estimated transfer function is: ');
tf(pitch_estimated_tf)
pitch_params=getpvec(pitch_estimated_tf);

if(np==1)
    pitch_gain=pitch_params(1)/pitch_params(2);
    pitch_tau=1/pitch_params(2);
    fprintf('pitch gain=%.3f, tau=%.3f\n',pitch_gain,pitch_tau);
elseif(np==2)
    pitch_omega=sqrt(pitch_params(3));
    pitch_gain=pitch_params(1)/pitch_params(3);
    pitch_damping=pitch_params(2)/(2*pitch_omega);
    fprintf('pitch omega=%.3f, gain=%.3f damping=%.3f\n',pitch_omega,pitch_gain,pitch_damping);
end

figure('Name','System analysis (yawrate)');
subplot(211);
bode(pitch_estimated_tf); 
grid;
title('pitch bode plot');

% subplot(312);
% rlocusplot(pitch_estimated_tf); 
% grid;
% title('pitch RootLucas plot');

subplot(212);
step(pitch_estimated_tf); 
grid;
title('pitch step response plot');
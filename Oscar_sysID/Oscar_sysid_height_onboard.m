% Sebastien Petit 
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% root on git clone https://github.com/ethz-asl/mav_system_identification -b DJI_M100_sysID
% This script can identified dynamic of roll with two sets of data VERTICAL VELOCITY
% DJI vc channel, 1=pitch, 2=roll, 4=vertical velocity, 3=yaw_rate.
% rpy = euler angle
% use xcorr if delay between datasets

clear all;
close all;
clc;
path(path, '../read_bags');
path(path, '../helper_functions');

%% Assume 1st  order system (!!!choice order!!!)  
np = 2; % number of poles [2]
nz = 1; % number of zeros [1]

%% load bag file, topics, infos...
bagfile_exp2 =  '../bags/thrust.bag';
topic_imu = '/dji_ros/imu';
topic_rcdata = '/dji_ros/rc';
topic_vel = '/dji_ros/vel';
bag2 = ros.Bag(bagfile_exp2);
bag2.info

%% Prepare datasets
Experiment2.IMU = readImu(bag2, topic_imu);
Experiment2.RCData = readRCDataFromJoyMessage(bag2, topic_rcdata);
Experiment2.Vel = readDJI_Velocity(bag2,topic_vel);

%% Write the quaternions, w, x,y,z order!!
Experiment2.IMU.q = [Experiment2.IMU.q(4,:); Experiment2.IMU.q(1,:); Experiment2.IMU.q(2,:); Experiment2.IMU.q(3,:)];

Experiment2.rpy_imu = quat2rpy(Experiment2.IMU.q);
Experiment2.RCData.t = Experiment2.RCData.t - Experiment2.RCData.t(1);
Experiment2.IMU.t = Experiment2.IMU.t - Experiment2.IMU.t(1);
Experiment2.Vel.t = Experiment2.Vel.t - Experiment2.Vel.t(1);

%% IMU, RCDATA interpolation with respect to onboard vz
Experiment2.RCData.roll = Experiment2.RCData.axes(1,:); % RPYT struct
Experiment2.RCData.pitch = Experiment2.RCData.axes(2,:);
Experiment2.RCData.yaw_rate = Experiment2.RCData.axes(3,:);
Experiment2.RCData.thrust(3,:) = Experiment2.RCData.axes(4,:);

imu_q1_intp = interp1(Experiment2.IMU.t,Experiment2.IMU.q(1,:),Experiment2.Vel.t,'spline'); % interp quaternion
imu_q2_intp = interp1(Experiment2.IMU.t,Experiment2.IMU.q(2,:),Experiment2.Vel.t,'spline');
imu_q3_intp = interp1(Experiment2.IMU.t,Experiment2.IMU.q(3,:),Experiment2.Vel.t,'spline');
imu_q4_intp = interp1(Experiment2.IMU.t,Experiment2.IMU.q(4,:),Experiment2.Vel.t,'spline');

cmd_ch1_intp = interp1(Experiment2.RCData.t,Experiment2.RCData.roll,Experiment2.Vel.t,'spline'); % interp RPYT
cmd_ch2_intp = interp1(Experiment2.RCData.t,Experiment2.RCData.pitch,Experiment2.Vel.t,'spline');
cmd_ch3_intp = interp1(Experiment2.RCData.t,Experiment2.RCData.yaw_rate,Experiment2.Vel.t,'spline');
cmd_ch4_intp = interp1(Experiment2.RCData.t,Experiment2.RCData.thrust(3,:),Experiment2.Vel.t,'spline');

Experiment2.IMU.q = zeros(4,size(imu_q1_intp,2));
Experiment2.IMU.q = [imu_q1_intp;imu_q2_intp;imu_q3_intp;imu_q4_intp]; % quaternion after interpo.
Experiment2.IMU.t = zeros(1,size(imu_q1_intp,2));
Experiment2.IMU.t = Experiment2.Vel.t;

Experiment2.RCData.channel = zeros(4,size(cmd_ch1_intp,2));
Experiment2.RCData.channel = [cmd_ch1_intp;cmd_ch2_intp;cmd_ch3_intp;cmd_ch4_intp]; %roll, pitch, yaw rate, vertical vel
Experiment2.RCData.t = zeros(1,size(cmd_ch1_intp,2));
Experiment2.RCData.t = Experiment2.Vel.t;
Experiment2.RCData = rmfield(Experiment2.RCData,'i');
Experiment2.RCData = rmfield(Experiment2.RCData,'roll');
Experiment2.RCData = rmfield(Experiment2.RCData,'pitch');
Experiment2.RCData = rmfield(Experiment2.RCData,'yaw_rate');
Experiment2.RCData = rmfield(Experiment2.RCData,'thrust');

%% scaling (lool "scaling.m") - delete transitory (no take begin and end part)
% specific scale of remote control is compute before Matlab process here 
DisplayPlot = 1; % before scale_roll = 25 * pi/180
[scale_thrust] = scalingAll(Experiment2.RCData.channel(4,:), Experiment2.Vel.vz , Experiment2.RCData.t, Experiment2.IMU.t, DisplayPlot);

Experiment2.thrust_cmd  = (Experiment2.RCData.channel(4,:)) * scale_thrust;

%% Plot vertical velocity from experiment 1
figure;
subplot(1,1,1);
plot(Experiment2.Vel.t, Experiment2.Vel.vz, Experiment2.RCData.t, Experiment2.thrust_cmd,'g--', 'linewidth', 2);
xlabel('time');
legend('y','y_{ref}');
ylabel('m/s');
title('dz from IMU and cmd(after regression/scaling) Just EXP 1');

%% Identification of vertical velocity system
%Control parameters
delay=[]; NaN; % [] menas no time delay or NaN for enabling delay estimation.

%% The length of data may vary.
Experiment2.t = Experiment2.RCData.t;
Experiment2.u1 = Experiment2.thrust_cmd;
Experiment2.y1 = Experiment2.Vel.vz;
Experiment2.Ts = mean(diff(Experiment2.Vel.t));

%% get rid of first and last 10 seconds (to remove ground and transient effects)*
% Experiment2.u1 = Experiment2.u1(Experiment2.t>10 & Experiment2.t < Experiment2.t(end)-49);
% Experiment2.y1 = double(Experiment2.y1(Experiment2.t>10 & Experiment2.t < Experiment2.t(end)-49));

thrust_data2 = iddata(Experiment2.y1',Experiment2.u1',Experiment2.Ts, ...
    'ExperimentName', 'FireFlySysID_1', 'InputName','thrust_{cmd}', ...
    'OutputName','thrust', 'InputUnit','rad', 'OutputUnit','rad', ...
    'TimeUnit','Second');
thrust_data2 = detrend(thrust_data2);

%% Generate model using Experiment2 and validate the model with Experiment2
thrust_estimated_tf = tfest(thrust_data2,np, nz);
[~, fit, ~] = compare(thrust_data2, thrust_estimated_tf);

figure;
compare(thrust_data2, thrust_estimated_tf);
grid on;

%% Estimated Transfer Functions    
disp('Thrust estimated transfer function is: ');
tf(thrust_estimated_tf)
thrust_params=getpvec(thrust_estimated_tf);

%% Estimated Transfer Functions
if(np==1)
    thrust_gain = thrust_params(1)/thrust_params(2);
    thrust_tau = 1/thrust_params(2);
    fprintf('thrust gain=%.3f, tau=%.3f\n',thrust_gain,thrust_tau);
elseif(np==2)
    thrust_params = getpvec(thrust_estimated_tf);
    thrust_omega = sqrt(thrust_params(3));
    thrust_gain = thrust_params(1)/thrust_params(3);
    thrust_damping = thrust_params(2)/(2*thrust_omega);
    fprintf('thrust omega=%.3f, gain=%.3f damping=%.3f\n',thrust_omega,thrust_gain,thrust_damping);
end

figure('Name','System analysis (dz)');
subplot(211);
bode(thrust_estimated_tf); 
grid;
title('dz bode plot');

% subplot(312);
% rlocusplot(thrust_estimated_tf); 
% grid;
% title('roll RootLucas plot');

subplot(212);
step(thrust_estimated_tf); 
grid;
title('dz step response plot');
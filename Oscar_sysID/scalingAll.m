% Sebastien Petit 
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% this function can compute scaling between 2 sets of data
% you must provide input --> commandes and sensors data + time data + selecting sample range
% data_sensors [rad] and [m/s] for dz / data_time [seconds]

function [scale] = scalingAll(data_cmd, data_sensors, data_time_cmd, data_time_sensors, DisplayPlot )

%% plot before regression
if(DisplayPlot)
figure('Name','Before regression');
plot(data_time_cmd,data_cmd,'b');
hold on;
plot(data_time_sensors,data_sensors,'r');
xlabel('Time(s)');
ylabel('rad or m/s');
legend('cmd','sensor');
grid on;
end

%% nonlinear optimisation for dz
% replace Off by Iter if you want see details 
f=@(x)sum((data_sensors - x*data_cmd).^2);
options = optimset('Display','off','MaxIter',2000,...
                   'MaxFunEvals',20000,...
                   'TolFun',1e-99,...
                   'TolX',1e-99);
th=0.1;
[scale,fval,exitflag] = fminsearch(f,th,options);
fprintf('scale=%f\n',scale);

%% plot after regression
if(DisplayPlot)
figure('Name','After regression');
plot(data_time_cmd,data_cmd*scale,'b');
hold on;
plot(data_time_sensors,data_sensors,'r');
xlabel('Time(s)');
ylabel('rad or m/s');
legend('cmd','sensor');
grid on;
end

end

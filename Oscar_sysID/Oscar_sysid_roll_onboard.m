% Sebastien Petit 
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% root on git clone https://github.com/ethz-asl/mav_system_identification -b DJI_M100_sysID
% This script can identified dynamic of roll with two sets of data ROLL
% DJI vc channel, 1=pitch, 2=roll, 4=vertical velocity, 3=yaw_rate.
% rpy = euler angle
% use xcorr if delay between datasets

clear all;
close all;
clc;
path(path, '../read_bags');
path(path, '../helper_functions');

%% Assume 1st  order system (!!!choice order!!!)  
np = 2; % number of poles [2]
nz = 1; % number of zeros [1]

%% two experiments are needed to validate the identification
bagfile_exp1 =  '../bags/roll.bag'; % give name of bag
bagfile_exp2 =  '../bags/roll.bag';

topic_imu = '/dji_ros/imu'; % find name of topic
topic_vcdata = '/dji_ros/rc';

bag1 = ros.Bag(bagfile_exp1); % read bag
bag2 = ros.Bag(bagfile_exp2);

bag1.info % get info of bag (topic, variables...)
bag2.info

%% Prepare datasets / extract data from Ros bag
Experiment1.IMU = readImu(bag1, topic_imu);
Experiment1.RCData = readRCDataFromJoyMessage(bag1, topic_vcdata);
Experiment2.IMU = readImu(bag2, topic_imu);
Experiment2.RCData = readRCDataFromJoyMessage(bag2, topic_vcdata);

%% Write the quaternions properly
Experiment1.IMU.q = [Experiment1.IMU.q(4,:); Experiment1.IMU.q(1,:); Experiment1.IMU.q(2,:); Experiment1.IMU.q(3,:)];
Experiment2.IMU.q = [Experiment2.IMU.q(4,:); Experiment2.IMU.q(1,:); Experiment2.IMU.q(2,:); Experiment2.IMU.q(3,:)];

%% time from 0 EXP 1 and EXP 2
Experiment1.RCData.t = Experiment1.RCData.t - Experiment1.RCData.t(1);
Experiment1.IMU.t = Experiment1.IMU.t - Experiment1.IMU.t(1);

Experiment2.RCData.t = Experiment2.RCData.t - Experiment2.RCData.t(1);
Experiment2.IMU.t = Experiment2.IMU.t - Experiment2.IMU.t(1);

%% RCDATA interpolation with respect to IMU EXP 1
Experiment1.RCData.roll = Experiment1.RCData.axes(1,:); % data roll 
cmd_roll_intp = interp1(Experiment1.RCData.t,Experiment1.RCData.roll,Experiment1.IMU.t,'spline'); % data cmd roll
Experiment1.IMU.q = [Experiment1.IMU.q(1,:);Experiment1.IMU.q(2,:);Experiment1.IMU.q(3,:);Experiment1.IMU.q(4,:)]; % data quaternion
Experiment1.RCData.roll = cmd_roll_intp;
Experiment1.RCData.t = zeros(1,size(cmd_roll_intp,2));
Experiment1.RCData.t = Experiment1.IMU.t;

%% RCDATA interpolation with respect to IMU EXP 2
Experiment2.RCData.roll = Experiment2.RCData.axes(1,:);
cmd_roll_intp = interp1(Experiment2.RCData.t,Experiment2.RCData.roll,Experiment2.IMU.t,'spline');
Experiment2.IMU.q = [Experiment2.IMU.q(1,:);Experiment2.IMU.q(2,:);Experiment2.IMU.q(3,:);Experiment2.IMU.q(4,:)];
Experiment2.RCData.roll=cmd_roll_intp;
Experiment2.RCData.t = zeros(1,size(cmd_roll_intp,2));
Experiment2.RCData.t = Experiment2.IMU.t;
Experiment1.rpy_imu = quat2rpy(Experiment1.IMU.q);
Experiment2.rpy_imu = quat2rpy(Experiment2.IMU.q);

%% scaling (lool "scalingAll.m") - delete transitory (no take begin and end part)
% specific scale of remote control is compute before Matlab process here 
DisplayPlot = 1; % before scale_roll = 25 * pi/180 rcMax remote control scale
[scale_roll] = scalingAll(Experiment1.RCData.roll, Experiment1.rpy_imu(1,:),Experiment1.RCData.t, Experiment1.IMU.t, DisplayPlot);
Experiment1.roll_cmd = (Experiment1.RCData.roll) * scale_roll;
Experiment2.roll_cmd = (Experiment2.RCData.roll) * scale_roll; % use same scale

%% Plot attitude from experiment 1
figure;
subplot(2,1,1);
plot(Experiment1.IMU.t, Experiment1.rpy_imu(1,:)*180/pi, Experiment1.RCData.t, Experiment1.roll_cmd*180/pi,'g--', 'linewidth', 2);
xlabel('time');
legend('y','y_{ref}');
ylabel('roll [deg]');
title('roll from IMU and cmd(after regression/scaling) EXP 1');

%% Plot attitude from experiment 2
subplot(2,1,2);
plot(Experiment2.IMU.t, Experiment2.rpy_imu(1,:)*180/pi, Experiment2.RCData.t, Experiment2.roll_cmd*180/pi,'g--', 'linewidth', 2);
legend('y','y_{ref}');
xlabel('time');
ylabel('roll [deg]');
title('roll from IMU and cmd(after regression/scaling) EXP 2');

%% Identification of roll system
%Control parameters
delay=[]; NaN; % [] menas no time delay or NaN for enabling delay estimation.

%% The length of data may vary. (1)
Experiment1.t = Experiment1.RCData.t;
Experiment1.u1 = Experiment1.roll_cmd;
Experiment1.y1 = Experiment1.rpy_imu(1,:);
Experiment1.Ts = mean(diff(Experiment1.t)); % sample time

%% get rid of first and last 10 seconds (to remove ground and transient effects) (1)
Experiment1.u1 = Experiment1.u1(Experiment1.t>10 & Experiment1.t < Experiment1.t(end)-10);
Experiment1.y1 = Experiment1.y1(Experiment1.t>10 & Experiment1.t < Experiment1.t(end)-10);
Experiment1.t = Experiment1.t(Experiment1.t>10 & Experiment1.t < Experiment1.t(end)-10);

%% uniformly sampled, and the input and output data for each experiment must be recorded at the same time instants (1)
[roll,lag_roll] = xcorr(Experiment1.u1,Experiment1.y1);
[~,I_roll] = max(roll);
time_sync_roll = lag_roll(I_roll);
roll_data1 = iddata(Experiment1.y1',Experiment1.u1',Experiment1.Ts, ...
    'ExperimentName', 'OscarSysID_1', 'InputName','roll_{cmd}', ...
    'OutputName','roll', 'InputUnit','rad', 'OutputUnit','rad', ...
    'TimeUnit','Second');
roll_data1 = detrend(roll_data1);

%% The length of data may vary. (2)
Experiment2.t = Experiment2.RCData.t;
Experiment2.u1 = Experiment2.roll_cmd;
Experiment2.y1 = Experiment2.rpy_imu(1,:);
Experiment2.Ts = mean(diff(Experiment2.t));

%% get rid of first and last 10 seconds (to remove ground and transient effects) (2)
Experiment2.u1 = Experiment2.u1(Experiment2.t>10 & Experiment2.t < Experiment2.t(end)-10);
Experiment2.y1 = Experiment2.y1(Experiment2.t>10 & Experiment2.t < Experiment2.t(end)-10);

%% uniformly sampled, and the input and output data for each experiment must be recorded at the same time instants (1)
roll_data2 = iddata(Experiment2.y1',Experiment2.u1',Experiment2.Ts,...
    'ExperimentName', 'OscarSysID_2', 'InputName','roll_{cmd}',...
    'OutputName','roll', 'InputUnit','rad', 'OutputUnit','rad',...
    'TimeUnit','Second');
roll_data2 = detrend(roll_data2);   

%% At this point we have 3 options !
% # 1 - Estimate a model from both experiments - but cannot validate it on independent dataset
% # 2 - Estimate a model from Exp1 and validate it on data from Exp2
% # 3 - Estimate a model from Exp2 and validate it on data from Exp1

%% Generate model using Experiment1 and validate the model with Experiment2
roll_estimated_tf1 = tfest(roll_data1, np, nz,delay); % uncomment for see transfert function
[~, fit1, ~] = compare(roll_data2, roll_estimated_tf1);

%% Generate model using Experiment2 and validate the model with Experiment1
roll_estimated_tf2 = tfest(roll_data2, np, nz,delay); % uncomment for see transfert function
[~, fit2, ~] = compare(roll_data1, roll_estimated_tf2);

if fit1>fit2
    %We pick the first Identification
    roll_estimated_tf = roll_estimated_tf1;
    disp('The roll model is estimated using experiment 1 and validated on data from experiment 2');
    figure;
    compare(roll_data2, roll_estimated_tf1);
    disp(strcat('The roll model fits the validation data with **',num2str(fit1), '** %'));
else
    % We pick the second Identification
    roll_estimated_tf = roll_estimated_tf2;
    disp('The roll model is estimated using experiment 2 and validated on data from experiment 1');
    figure;
    compare(roll_data1, roll_estimated_tf2);
    disp(strcat('The roll model fits the validation data with **',num2str(fit2), '** %'));
end

%% Estimated Transfer Functions
disp('roll estimated transfer function is: ');
tf(roll_estimated_tf)
roll_params = getpvec(roll_estimated_tf);

if(np==1)
    roll_gain = roll_params(1)/roll_params(2);
    roll_tau = 1/roll_params(2);
    fprintf('roll gain=%.3f, tau=%.3f\n',roll_gain,roll_tau);
elseif(np==2)
    roll_omega = sqrt(roll_params(3));
    roll_gain = roll_params(1)/roll_params(3);
    roll_damping = roll_params(2)/(2*roll_omega);
    fprintf('roll omega=%.3f, gain=%.3f damping=%.3f\n',roll_omega,roll_gain,roll_damping);
end

figure('Name','System analysis (yawrate)');
subplot(211);
bode(roll_estimated_tf); 
grid;
title('roll bode plot');

% subplot(312);
% rlocusplot(roll_estimated_tf); 
% grid;
% title('roll RootLucas plot');

subplot(212);
step(roll_estimated_tf); 
grid;
title('roll step response plot');
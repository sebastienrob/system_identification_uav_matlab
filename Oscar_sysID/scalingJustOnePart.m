% Sebastien Petit 
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% this function can compute scaling between 2 sets of data
% you must provide input --> commandes and sensors data + time data + selecting sample range
% data_sensors [rad] / data_time [seconds]

function [scale] = scaling(data_cmd, data_sensors, data_time_cmd, data_time_sensors, start_t, end_t, DisplayPlot )

%% sample time sensor
ts = mean(diff(data_time_sensors));

%% plot before regression
if(DisplayPlot)
figure('Name','Before regression');
plot(data_time_cmd,data_cmd,'b');
hold on;
plot(data_time_sensors,data_sensors,'r');
xlabel('Time(s)');
ylabel('rad or m/s');
legend('cmd','sensor');
grid on;
end
 
%% scale rc time with sensor time 
[roll,lag_roll] = xcorr(data_cmd,data_sensors);
[~,I_roll] = max(roll);
time_sync_roll = lag_roll(I_roll);
data_time_cmd = data_time_cmd-time_sync_roll*ts;

%% sample range of sensor&cmd data (cause start_t<time<end_t)
b_idx = min(find(abs(data_time_cmd - start_t)< ts));
e_idx = min(find(abs(data_time_cmd - end_t)< ts));
samp_range_u = b_idx:e_idx;

b_idx = min(find(abs(data_time_sensors - start_t)< ts));
e_idx = min(find(abs(data_time_sensors - end_t)< ts));
samp_range_y = b_idx:e_idx;

%% detrend --> removes the best straight-fit line from the data 
u_data = detrend(data_cmd(samp_range_u));
%u_t = data_time_cmd(samp_range_u);
y_data = detrend(data_sensors(samp_range_y));
%y_t = data_time_sensors(samp_range_y);

%% nonlinear optimisation 
f=@(x)sum((y_data - x*u_data).^2);
options = optimset('Display','Iter','MaxIter',2000,... % replace off by Iter for see intermediate steps
                   'MaxFunEvals',20000,...
                   'TolFun',1e-99,...
                   'TolX',1e-99);
th=0.1;
[scale,fval,exitflag] = fminsearch(f,th,options);
fprintf('scale=%f\n',scale);

%% plot after regression
if(DisplayPlot)
figure('Name','After regression');
plot(data_time_cmd,data_cmd*scale,'b');
hold on;
plot(data_time_sensors,data_sensors,'r');
xlabel('Time(s)');
ylabel('rad or m/s');
strLeg_Roll=sprintf('scale*%f, range from %.2fs ~ %.2fs',scale,data_time_cmd(b_idx),data_time_cmd(e_idx));
legend(strLeg_Roll,'IMU mea');
grid on;
end

end
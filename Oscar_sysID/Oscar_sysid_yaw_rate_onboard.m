% Sebastien Petit 
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% root on git clone https://github.com/ethz-asl/mav_system_identification -b DJI_M100_sysID
% This script can identified dynamic of roll with two sets of data YAW RATE
% DJI vc channel, 1=pitch, 2=roll, 4=vertical velocity, 3=yaw_rate.
% rpy = euler angle
% use xcorr if delay between datasets

clear all;
close all;
clc;
path(path, '../read_bags');
path(path, '../helper_functions');

%% Assume 1st  order system (!!!choice order!!!)  
np = 1; % number of poles [2]
nz = 0; % number of zeros [1]

%% two experiments are needed to validate the identification
bagfile_exp1 =  '../bags/yaw_rate.bag'; % give name of bag
bagfile_exp2 =  '../bags/yaw_rate.bag';

topic_imu = '/dji_ros/imu'; % find name of topic
topic_vcdata = '/dji_ros/rc';

bag1 = ros.Bag(bagfile_exp1);
bag2 = ros.Bag(bagfile_exp2);

bag1.info % get info of bag (topic, variables...)
bag2.info

%% Prepare datasets / extract data from Ros bag
Experiment1.IMU = readImu(bag1, topic_imu);
Experiment1.RCData = readRCDataFromJoyMessage(bag1, topic_vcdata);
Experiment2.IMU = readImu(bag2, topic_imu);
Experiment2.RCData = readRCDataFromJoyMessage(bag2, topic_vcdata);

%% Write the quaternions properly
Experiment1.IMU.q = [Experiment1.IMU.q(4,:); Experiment1.IMU.q(1,:); Experiment1.IMU.q(2,:); Experiment1.IMU.q(3,:)];
Experiment2.IMU.q = [Experiment2.IMU.q(4,:); Experiment2.IMU.q(1,:); Experiment2.IMU.q(2,:); Experiment2.IMU.q(3,:)];

%% time from 0 EXP 1 and EXP 2
Experiment1.RCData.t = Experiment1.RCData.t - Experiment1.RCData.t(1);
Experiment1.IMU.t = Experiment1.IMU.t - Experiment1.IMU.t(1);

Experiment2.RCData.t = Experiment2.RCData.t - Experiment2.RCData.t(1);
Experiment2.IMU.t = Experiment2.IMU.t - Experiment2.IMU.t(1);

%% RCDATA interpolation with respect to IMU EXP 1
Experiment1.RCData.yaw_rate = Experiment1.RCData.axes(3,:);
cmd_yawrate_intp = interp1(Experiment1.RCData.t,Experiment1.RCData.yaw_rate,Experiment1.IMU.t,'spline');
Experiment1.IMU.q = [Experiment1.IMU.q(1,:);Experiment1.IMU.q(2,:);Experiment1.IMU.q(3,:);Experiment1.IMU.q(4,:)];
Experiment1.RCData.yaw_rate = cmd_yawrate_intp;
Experiment1.RCData.t = zeros(1,size(cmd_yawrate_intp,2));
Experiment1.RCData.t = Experiment1.IMU.t;
Experiment1.RCData.yaw_rate = -Experiment1.RCData.yaw_rate;

%% RCDATA interpolation with respect to IMU EXP 2
Experiment2.RCData.yaw_rate = Experiment2.RCData.axes(3,:);
cmd_yawrate_intp = interp1(Experiment2.RCData.t,Experiment2.RCData.yaw_rate,Experiment2.IMU.t,'spline');
Experiment2.IMU.q = [Experiment2.IMU.q(1,:);Experiment2.IMU.q(2,:);Experiment2.IMU.q(3,:);Experiment2.IMU.q(4,:)];
Experiment2.RCData.yaw_rate = cmd_yawrate_intp;
Experiment2.RCData.t = zeros(1,size(cmd_yawrate_intp,2));
Experiment2.RCData.t = Experiment2.IMU.t;
Experiment1.rpy_imu = quat2rpy(Experiment1.IMU.q);
Experiment2.rpy_imu = quat2rpy(Experiment2.IMU.q);
Experiment2.RCData.yaw_rate = -Experiment2.RCData.yaw_rate; % sign change !!!!!!!

%% scaling (lool "scalingAll.m") - delete transitory (no take begin and end part)
% specific scale of remote control is compute before Matlab process here 
DisplayPlot = 1; % before scale_roll = 150 * pi/180
[scale_yaw_rate] = scalingAll(Experiment1.RCData.yaw_rate, Experiment1.IMU.w(3,:),Experiment1.RCData.t, Experiment1.IMU.t, DisplayPlot);

Experiment1.yaw_cmd = (Experiment1.RCData.yaw_rate) * scale_yaw_rate;
Experiment2.yaw_cmd = (Experiment2.RCData.yaw_rate) * scale_yaw_rate; % use same scale

%% Plot yaw rate from experiment 1
figure;
subplot(2,1,1);
plot(Experiment1.IMU.t, Experiment1.IMU.w(3,:)*180/pi, Experiment1.RCData.t, Experiment1.yaw_cmd*180/pi, 'g--', 'linewidth', 2);
xlabel('time');
legend('y','y_{ref}');
ylabel('yaw rate [deg]/s');
title('yaw_rate from IMU and cmd(after regression/scaling) EXP 1');

%% Plot yaw rate from experiment 2
subplot(2,1,2);
plot(Experiment2.IMU.t, Experiment2.IMU.w(3,:)*180/pi, Experiment2.RCData.t, Experiment2.yaw_cmd*180/pi, 'g--', 'linewidth', 2);
xlabel('time');
legend('y','y_{ref}');
ylabel('yaw rate [deg]/s');
title('yaw_rate from IMU and cmd(after regression/scaling) EXP 2');

%% Identification of roll system
delay=[]; NaN; % [] menas no time delay or NaN for enabling delay estimation.

%% The length of data may vary.
Experiment1.t =  Experiment1.RCData.t;
Experiment1.u1 = Experiment1.yaw_cmd;
Experiment1.y1 = Experiment1.IMU.w(3,:);
Experiment1.Ts = mean(diff(Experiment1.t));

%% validate
%% Choose valid data range. (yaw variation duration)
Experiment1.u1 = Experiment1.u1(Experiment1.t>10 & Experiment1.t < Experiment1.t(end)-10);
Experiment1.y1 = Experiment1.y1(Experiment1.t>10 & Experiment1.t < Experiment1.t(end)-10);
yawrate_data1 = iddata(Experiment1.y1',Experiment1.u1',Experiment1.Ts, ...
    'ExperimentName', 'Oscar_sysID_yaw1', 'InputName','yawrate_{cmd}', ...
    'OutputName','yawrate', 'InputUnit','rad', 'OutputUnit','rad', ...
    'TimeUnit','Second');
yawrate_data1 = detrend(yawrate_data1);

%% The length of data may vary.
Experiment2.t = (Experiment2.IMU.t + Experiment2.RCData.t)/2;
Experiment2.u1 = Experiment2.yaw_cmd;
Experiment2.y1 = Experiment2.IMU.w(3,:);
Experiment2.Ts = mean(diff(Experiment2.IMU.t));

%% Choose valid data range. (yaw variation duration)
Experiment2.u1 = Experiment2.u1(Experiment2.t>10 & Experiment2.t < Experiment2.t(end)-10);
Experiment2.y1 = Experiment2.y1(Experiment2.t>10 & Experiment2.t < Experiment2.t(end)-10);

yawrate_data2 = iddata(Experiment2.y1',Experiment2.u1',Experiment2.Ts, ...
    'ExperimentName', 'Oscar_sysID_yaw2', 'InputName','yawrate_{cmd}', ...
    'OutputName','yawrate', 'InputUnit','rad', 'OutputUnit','rad', ...
    'TimeUnit','Second');

%% Generate model using Experiment1 and validate the model with Experiment2
yawrate_estimated_tf1 = tfest(yawrate_data1,np, nz);
[~, fit1, ~] = compare(yawrate_data2, yawrate_estimated_tf1);

%% Generate model using Experiment2 and validate the model with Experiment1
yawrate_estimated_tf2 = tfest(yawrate_data2,np, nz);
[~, fit2, ~] = compare(yawrate_data1, yawrate_estimated_tf2);

if fit1>fit2
    %We pick the first Identification
    yawrate_estimated_tf = yawrate_estimated_tf1;
    disp('The yawrate model is estimated using experiment 1 and validated on data from experiment 2');
    figure;
    compare(yawrate_data2, yawrate_estimated_tf1);
    disp(strcat('The yawrate model fits the validation data with **',...
        num2str(fit1), '** %'));
else
    %We pick the second Identification
    yawrate_estimated_tf = yawrate_estimated_tf2;
    disp('The yawrate model is estimated using experiment 2 and validated on data from experiment 1');
    figure;
    compare(yawrate_data1, yawrate_estimated_tf2);
    disp(strcat('The yawrate model fits the validation data with **',...
        num2str(fit2), '** %'));
end

%% Estimated Transfer Functions
disp('yawrate estimated transfer function is: ');
tf(yawrate_estimated_tf)
yawrate_params=getpvec(yawrate_estimated_tf);

if(np==1)
    yawrate_gain=yawrate_params(1)/yawrate_params(2);
    yawrate_tau=1/yawrate_params(2);
    fprintf('yawrate gain=%.3f, tau=%.3f\n',yawrate_gain,yawrate_tau);
elseif(np==2)
    yawrate_omega=sqrt(yawrate_params(3));
    yawrate_gain=yawrate_params(1)/yawrate_params(3);
    yawrate_damping=yawrate_params(2)/(2*yawrate_omega);
    fprintf('yawrate omega=%.3f, gain=%.3f damping=%.3f\n',yawrate_omega,yawrate_gain,yawrate_damping);
end

figure('Name','System analysis (yawrate)');
subplot(211);
bode(yawrate_estimated_tf); 
grid;
title('yawrate bode plot');

% subplot(312);
% rlocusplot(yawrate_estimated_tf); grid;
% title('yawrate RootLucas plot');

subplot(212);
step(yawrate_estimated_tf); 
grid;
title('yawrate step response plot');
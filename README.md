# Dynamical System Identification of an UAV Quadrorotor 
Matlab scripts to perform system identification for an UAV.  
Based of project on https://github.com/ethz-asl/mav_system_identification.  

This repository depends on matlab_rosbag (https://github.com/bcharrow/matlab_rosbag/releases)  
!!!here, the library is already installed!!!  
Before you have need to add this :  
* Right click on *matlab_rosbag-0.4-linux64* folder in matlab  
* Add to Path --> Selected Folders and subfolders

this repository contains six folders :  
* *Oscar_sysID* : perform system identification for an UAV  
* *bags* : here you can add your ROS BAGs  
* *read_bags* : contains functions to read specific message type  
* *matlab_rosbag-0.4-linux64* : library for reading ROS bags in Matlab  
* *Oscar_DownloadVideoImageFromeBag* : specific folder to retrieve images/video from a camera   
* *helper_functions* : various functions to simplify script  

## Oscar_sysID - files
In this folder you can perform system identification for an UAV  

* *scalingAll.m* : perform scalling (all data)  
* *scalingJustOnePart.m* : perform scalling (a part of data) MODIFY AS NEEDED   
* *MIMO_SeeCoupling.m* : just for see coupling between two set of data (don't use for moment)  

* *Oscar_sysid_roll_onboard.m* : system identification for Roll  
* *Oscar_sysid_pitch_onboard.m* : system identification for Pitch  
* *Oscar_sysid_yaw_rate_onboard.m* : system identification for yaw rate  
* *Oscar_sysid_heigth_onboard.m* : system identification for vertical velocity (dz)  

## Oscar_sysID - explain
first you need to load your bag file into matlab.  
Then using the bag.info command identify the necessary topics and use the library *read_bags*  
At the start of the script specified the order for identification (poles and zeros)  
use interp1 to get your data to the same length  
ScallingAll function perform to scale your two dataset (replace 'off' by 'Iter' in this script to see details)  
detrend cam remove means --> you build linear models that describe the responses for deviations from a physical equilibrium.  
With steady-state data, it is reasonable to assume that the mean levels of the signals correspond to such an equilibrium.  
Thus, you can seek models around zero without modeling the absolute equilibrium levels in physical units.  
push your data as object with iddata and perform identification with tfest function (get transfert function)  
We get result and fit (compare) after inject input data inside dynamical model.  
After we compute parameters (gain/tau for 1st order and omega/gain/damping for 2nd order)  

We get five figures for each identification :  

##### Data before scaling/regression
![data before scaling/regression](/ImageWiki/1.png "Before Regression")  

##### Data after scaling/regression
![data after scaling/regression](/ImageWiki/2.png "After Regression")  

##### Data after scaling/regression Sensors&cmd
![data after scaling/regression Sensors&cmd](/ImageWiki/3.png "After Regression Sensors&cmd")  

##### Result after enter input inside our dynamic system
![result after enter input inside our dynamic system](/ImageWiki/4.png "Simulated response comparaison")  

##### Identify how frequency input change Amplitude/phase of output
![identify how frequency input change Amplitude/phase of output](/ImageWiki/5.png "Bode diagramm")  

## Oscar_sysID - Matlab Toolbox
you can also use matlab toolbox to perform System identification.  
Identify Linear Models Using System Identification App :  
*https://fr.mathworks.com/help/ident/gs/identify-linear-models-using-the-gui.html*  

##### System Identification App - matlab toolbox
![System Identification App - matlab toolbox](/ImageWiki/6.png "System Identification App")  





